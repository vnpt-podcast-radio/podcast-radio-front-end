import { Helmet } from "react-helmet";
import Login from "../components/Login";

function login() {
  return (
    <>
      <Helmet>
        <title>VNPT Podcast | Đăng nhập</title>
      </Helmet>
      <Login />
    </>
  );
}

export default login;
