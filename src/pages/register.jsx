import { Helmet } from "react-helmet";
import Register from "../components/Register";

function register() {
  return (
    <>
      <Helmet>
        <title>VNPT Podcast | Đăng ký tài khoản</title>
      </Helmet>
      <Register />
    </>
  );
}

export default register;
