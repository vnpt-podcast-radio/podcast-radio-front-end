import "../index";
import { Layout, Space } from "antd";
import HeaderHome from "../components/Header";
import SidebarHome from "../components/Sidebar";
import HomeContent from "../components/home/HomeContent";
function home() {
  return (
    <Space
      direction="vertical"
      style={{ width: "100%", position: "absolute", top: 0 }}
    >
      <Layout style={{ backgroundColor: "#37075d" }}>
        <SidebarHome></SidebarHome>
        <Layout>
          <HeaderHome></HeaderHome>
          <HomeContent></HomeContent>
        </Layout>
      </Layout>
    </Space>
  );
}

export default home;
