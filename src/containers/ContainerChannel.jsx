import { Button, Input, Modal, Typography } from 'antd'
import React, { useState } from 'react'
import ChannelHeader from '../components/channel/ChannelHeader';
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import { v4 as uuidv4 } from 'uuid';
function ContainerChannel() {
    const [visible, setVisible] = useState(false);
    const [visibleDnd, setVisibleDnd] = useState(false);
    const itemsFromBackend = [
        { id: uuidv4(), content: "First task" },
        { id: uuidv4(), content: "Second task" },
        { id: uuidv4(), content: "Third task" },
        { id: uuidv4(), content: "Fourth task" },
        { id: uuidv4(), content: "Fifth task" },
        { id: uuidv4(), content: "Fifth task" },
        { id: uuidv4(), content: "Fifth task" },
        { id: uuidv4(), content: "Fifth task" }
    ];

    const columnsFromBackend = {
        [uuidv4()]: {
            name: "Data",
            items: itemsFromBackend
        },
        [uuidv4()]: {
            name: "To do",
            items: []
        },
    };

    const onDragEnd = (result, columns, setColumns) => {
        if (!result.destination) return;
        const { source, destination } = result;

        if (source.droppableId !== destination.droppableId) {
            const sourceColumn = columns[source.droppableId];
            const destColumn = columns[destination.droppableId];
            const sourceItems = [...sourceColumn.items];
            const destItems = [...destColumn.items];
            const [removed] = sourceItems.splice(source.index, 1);
            destItems.splice(destination.index, 0, removed);
            setColumns({
                ...columns,
                [source.droppableId]: {
                    ...sourceColumn,
                    items: sourceItems
                },
                [destination.droppableId]: {
                    ...destColumn,
                    items: destItems
                }
            });
        } else {
            const column = columns[source.droppableId];
            const copiedItems = [...column.items];
            const [removed] = copiedItems.splice(source.index, 1);
            copiedItems.splice(destination.index, 0, removed);
            setColumns({
                ...columns,
                [source.droppableId]: {
                    ...column,
                    items: copiedItems
                }
            });
        }
    };
    const [columns, setColumns] = useState(columnsFromBackend);
    return (
        <div
            style={{
                padding: '30px 30px 30px 40px',
                height: '100%',
            }}>
            <Modal
                style={{
                    backgroundColor: 'white'
                }}
                visible={visible}
                centered
                onOk={() => setVisible(false)}
                onCancel={() => setVisible(false)}
                width={1000}>
                <div
                    style={{
                        marginTop: '20px'
                    }}>
                    <Typography.Text>
                        Tieu de:
                    </Typography.Text>
                    <Input></Input>
                </div>
                <div
                    style={{
                        marginTop: '20px'
                    }}>
                    <Typography.Text>
                        Noi dung:
                    </Typography.Text>
                    <Input></Input>
                </div>
                <div
                    style={{
                        marginTop: '20px'
                    }}>
                    <Typography.Text>
                        The loai:
                    </Typography.Text>
                    <Input></Input>
                </div>
                <div
                    style={{
                        marginTop: '20px'
                    }}>
                    <Button onClick={() => setVisibleDnd(true)} type='primary'>
                        <Typography.Text style={{ color: 'white' }}>
                            Them nhac
                        </Typography.Text>
                    </Button>
                </div>
            </Modal>
            <Modal
                style={{
                    backgroundColor: 'white',
                }}
                bodyStyle={{
                    display: 'flex',
                    justifyContent: 'space-around',

                }}
                visible={visibleDnd}
                centered
                onOk={() => setVisibleDnd(false)}
                onCancel={() => setVisibleDnd(false)}
                width={1000}>
                <DragDropContext
                    onDragEnd={result => onDragEnd(result, columns, setColumns)}
                >
                    {Object.entries(columns).map(([columnId, column], index) => {
                        return (
                            <div
                                style={{
                                    backgroundColor: '#656267',
                                    display: "flex",
                                    flexDirection: "column",
                                    alignItems: "center"
                                }}
                                key={columnId}
                            >
                                <h2>{column.name}</h2>
                                <div
                                    className='slider-bar'
                                    style={{
                                        margin: 8,
                                        overflow: 'auto',
                                        minHeight: 500,
                                        maxHeight: 500
                                    }}>
                                    <Droppable
                                        droppableId={columnId}
                                        key={columnId}>
                                        {(provided, snapshot) => {
                                            return (
                                                <div
                                                    {...provided.droppableProps}
                                                    ref={provided.innerRef}
                                                    style={{
                                                        background: snapshot.isDraggingOver
                                                            ? "lightblue"
                                                            : "lightgrey",
                                                        padding: 4,
                                                        width: 250,
                                                        minHeight: 500
                                                    }}
                                                >
                                                    {column.items.map((item, index) => {
                                                        return (
                                                            <Draggable
                                                                key={item.id}
                                                                draggableId={item.id}
                                                                index={index}
                                                            >
                                                                {(provided, snapshot) => {
                                                                    return (
                                                                        <div
                                                                            ref={provided.innerRef}
                                                                            {...provided.draggableProps}
                                                                            {...provided.dragHandleProps}
                                                                            style={{
                                                                                userSelect: "none",
                                                                                padding: 16,
                                                                                margin: "0 0 8px 0",
                                                                                minHeight: "50px",
                                                                                backgroundColor: snapshot.isDragging
                                                                                    ? "#263B4A"
                                                                                    : "#456C86",
                                                                                color: "white",
                                                                                ...provided.draggableProps.style
                                                                            }}
                                                                        >
                                                                            {item.content}
                                                                        </div>
                                                                    );
                                                                }}
                                                            </Draggable>
                                                        );
                                                    })}
                                                    {provided.placeholder}
                                                </div>
                                            );
                                        }}
                                    </Droppable>
                                </div>
                            </div>
                        );
                    })}
                </DragDropContext>

            </Modal>
            <ChannelHeader onClickOpen={() => setVisible(true)}></ChannelHeader>
        </div>
    )
}
export default ContainerChannel

