import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./index.css";
import App from "./App";
import Login from "./pages/login";
import Register from "./pages/register";
import Home from "./pages/home";
import Room from "./pages/room";
import Channel from "./pages/channel";
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<App />} />
      <Route path="/login" element={<Login />} />
      <Route path="/register" element={<Register />} />
      <Route path="/home" element={<Home />} >
        <Route path="/home/:roomId" />
      </Route>
      <Route path="/home/room" element={<Room />} />
      <Route path="/channel" element={<Channel />}>
        <Route path="/channel/:key" />
      </Route>
    </Routes>
  </BrowserRouter>
);
