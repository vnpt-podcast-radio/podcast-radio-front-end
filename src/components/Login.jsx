import { Button, Checkbox, Form, Input, Typography } from "antd";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { useState } from "react";

const StyledLogin = styled.div`
  position: relative;
  padding: 60px;
  background: rgba(255, 255, 255, 0.25);
  backdrop-filter: blur(15px);
  border-bottom: 1px solid rgba(255, 255, 255, 0.25);
  border-right: 1px solid rgba(255, 255, 255, 0.25);
  border-radius: 20px;
  width: 500px;
  display: flex;
  flex-direction: column;
  gap: 30px;
  box-shadow: 0 25px 50px rgba(0, 0, 0, 0.1);
`;

const StyledLeaves = styled.div`
  position: absolute;
  width: 100%;
  height: 100vh;
  overflow: hidden;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 1;
  pointer-events: none;
`;

const StyledSet = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  pointer-events: none;
`;

const StyledLeaf = styled.div`
  position: absolute;
  display: block;
`;

const StyledInputBox = styled.div`
  position: relative;
`;

const StyledGroup = styled.div`
  display: flex;
  justify-content: space-between;
`;

const styles = {
  section: {
    position: "relative",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    height: "100vh",
    overflowX: "hidden",
  },

  leafChild1: {
    left: "20%",
    animation: "animate 20s linear infinite",
  },

  leafChild2: {
    left: "50%",
    animation: "animate 14s linear infinite",
  },

  leafChild3: {
    left: "70%",
    animation: "animate 12s linear infinite",
  },

  leafChild4: {
    left: "5%",
    animation: "animate 15s linear infinite",
  },

  leafChild5: {
    left: "85%",
    animation: "animate 18s linear infinite",
  },

  leafChild6: {
    left: "90%",
    animation: "animate 12s linear infinite",
  },

  leafChild7: {
    left: "15%",
    animation: "animate 14s linear infinite",
  },

  leafChild8: {
    left: "60%",
    animation: "animate 15s linear infinite",
  },

  bgSection: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    objectFit: "cover",
    pointerEvents: "none",
  },

  girlSection: {
    position: "absolute",
    transform: "scale(0.65)",
    pointerEvents: "none",
    animation: "animateGirl 10s linear infinite",
  },

  treesSection: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    objectFit: "cover",
    zIndex: "99",
    pointerEvents: "none",
  },

  inputBox: {
    position: "relative",
    width: "100%",
    padding: "15px 20px",
    outline: "none",
    fontSize: "1.25em",
    color: "#8f2c24",
    borderRadius: "5px",
    background: "#fff",
    border: "none",
  },

  titleLogin: {
    position: "relative",
    width: "100%",
    textAlign: "center",
    fontSize: "2.5em",
    fontWeight: 600,
    color: "#8f2c24",
  },

  btnSubmit: {
    position: "relative",
    width: "100%",
    height: "100%",
    padding: "15px 20px",
    fontSize: "1.25em",
    fontWeight: 500,
    color: "#fff",
    borderRadius: "5px",
    background: "#8f2c24",
    outline: "none",
    border: "none",
    transition: "0.5s",
  },

  textLink: {
    fontSize: "1.25em",
    color: "#8f2c24",
    fontWeight: "500",
    textDecoration: "none",
  },
};

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleLogin = () => {};

  return (
    <section style={styles.section}>
      <StyledLeaves>
        <StyledSet>
          <StyledLeaf style={styles.leafChild1}>
            <img
              src="https://cdn.discordapp.com/attachments/1034369224543567936/1084847813441110077/leaf_01.png"
              alt=""
            />
          </StyledLeaf>
          <StyledLeaf style={styles.leafChild2}>
            <img
              src="https://cdn.discordapp.com/attachments/1034369224543567936/1084847813696954449/leaf_02.png"
              alt=""
            />
          </StyledLeaf>
          <StyledLeaf style={styles.leafChild3}>
            <img
              src="https://cdn.discordapp.com/attachments/1034369224543567936/1084847813952815154/leaf_03.png"
              alt=""
            />
          </StyledLeaf>
          <StyledLeaf style={styles.leafChild4}>
            <img
              src="https://cdn.discordapp.com/attachments/1034369224543567936/1084847814183493743/leaf_04.png"
              alt=""
            />
          </StyledLeaf>
          <StyledLeaf style={styles.leafChild5}>
            <img
              src="https://cdn.discordapp.com/attachments/1034369224543567936/1084847813441110077/leaf_01.png"
              alt=""
            />
          </StyledLeaf>
          <StyledLeaf style={styles.leafChild6}>
            <img
              src="https://cdn.discordapp.com/attachments/1034369224543567936/1084847813696954449/leaf_02.png"
              alt=""
            />
          </StyledLeaf>
          <StyledLeaf style={styles.leafChild7}>
            <img
              src="https://cdn.discordapp.com/attachments/1034369224543567936/1084847813952815154/leaf_03.png"
              alt=""
            />
          </StyledLeaf>
          <StyledLeaf style={styles.leafChild8}>
            <img
              src="https://cdn.discordapp.com/attachments/1034369224543567936/1084847814183493743/leaf_04.png"
              alt=""
            />
          </StyledLeaf>
        </StyledSet>
      </StyledLeaves>

      <img
        src="https://cdn.discordapp.com/attachments/1034369224543567936/1084847812656767026/bg.jpg"
        alt=""
        style={styles.bgSection}
      />
      <img
        src="https://cdn.discordapp.com/attachments/1034369224543567936/1084847813155901560/girl.png"
        alt=""
        style={styles.girlSection}
      />
      <img
        src="https://cdn.discordapp.com/attachments/1034369224543567936/1084847814393217064/trees.png"
        alt=""
        style={styles.bgSection}
      />

      <StyledLogin>
        <Typography.Title level={2} style={styles.titleLogin}>
          Đăng Nhập
        </Typography.Title>

        <Form initialValues={{ remember: true }} scrollToFirstError>
          <StyledInputBox>
            <Form.Item
              label=""
              name="email"
              rules={[
                {
                  type: "email",
                  message: "Địa chỉ email không hợp lệ",
                },
                {
                  required: true,
                  message: "Vui lòng nhập email của bạn",
                },
              ]}
            >
              <Input
                placeholder="Email"
                onChange={(e) => setEmail(e.target.value)}
                style={styles.inputBox}
              />
            </Form.Item>
          </StyledInputBox>

          <StyledInputBox>
            <Form.Item
              label=""
              name="password"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập mật khẩu của bạn",
                },
              ]}
            >
              <Input.Password
                placeholder="Mật khẩu"
                onChange={(e) => setPassword(e.target.value)}
                style={styles.inputBox}
              />
            </Form.Item>
          </StyledInputBox>

          <StyledInputBox>
            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                onClick={handleLogin()}
                style={styles.btnSubmit}
              >
                ĐĂNG NHẬP
              </Button>
            </Form.Item>
          </StyledInputBox>

          <StyledGroup>
            <Form.Item name="remember">
              <Checkbox style={styles.textLink}>Nhớ tài khoản</Checkbox>
            </Form.Item>
            <Typography.Link href="#" style={styles.textLink}>
              Quên mật khẩu?
            </Typography.Link>
          </StyledGroup>

          <Typography.Paragraph style={{ textAlign: "center" }}>
            <Typography.Text style={styles.textLink}>
              Bạn chưa có tài khoản? <Link to="/register">Đăng ký ngay!</Link>
            </Typography.Text>
          </Typography.Paragraph>
        </Form>
      </StyledLogin>
    </section>
  );
};

export default Login;
