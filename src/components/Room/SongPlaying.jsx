import React from 'react'
import { Button, Image, Typography } from 'antd';
import ReactAudioPlayer from 'react-audio-player';

function SongPlaying() {
    const playList = [
        {
            id: "1",
            image:
                "https://photo-resize-zmp3.zmdcdn.me/w240_r1x1_jpeg/cover/b/1/a/a/b1aa55127e859a60291163b791000052.jpg",
            name: 'Chạy Về Khóc Với Anh',
            author: 'Erik, Duzme Remix'
        },
        {
            id: "2",
            image:
                "https://photo-resize-zmp3.zmdcdn.me/w240_r1x1_jpeg/cover/b/1/a/a/b1aa55127e859a60291163b791000052.jpg",
            name: 'Chạy Về Khóc Với Anh',
            author: 'Erik, Duzme Remix'
        },
        {
            id: "3",
            image:
                "https://photo-resize-zmp3.zmdcdn.me/w240_r1x1_jpeg/cover/b/1/a/a/b1aa55127e859a60291163b791000052.jpg",
            name: 'Chạy Về Khóc Với Anh',
            author: 'Erik, Duzme Remix'
        },
        {
            id: "4",
            image:
                "https://photo-resize-zmp3.zmdcdn.me/w240_r1x1_jpeg/cover/b/1/a/a/b1aa55127e859a60291163b791000052.jpg",
            name: 'Chạy Về Khóc Với Anh',
            author: 'Erik, Duzme Remix'
        },
        {
            id: "5",
            image:
                "https://photo-resize-zmp3.zmdcdn.me/w240_r1x1_jpeg/cover/b/1/a/a/b1aa55127e859a60291163b791000052.jpg",
            name: 'Chạy Về Khóc Với Anh',
            author: 'Erik, Duzme Remix'
        },
        {
            id: "6",
            image:
                "https://photo-resize-zmp3.zmdcdn.me/w240_r1x1_jpeg/cover/b/1/a/a/b1aa55127e859a60291163b791000052.jpg",
            name: 'Chạy Về Khóc Với Anh',
            author: 'Erik, Duzme Remix'
        },
        {
            id: "7",
            image:
                "https://photo-resize-zmp3.zmdcdn.me/w240_r1x1_jpeg/cover/b/1/a/a/b1aa55127e859a60291163b791000052.jpg",
            name: 'Chạy Về Khóc Với Anh',
            author: 'Erik, Duzme Remix'
        },
        {
            id: "8",
            image:
                "https://photo-resize-zmp3.zmdcdn.me/w240_r1x1_jpeg/cover/b/1/a/a/b1aa55127e859a60291163b791000052.jpg",
            name: 'Chạy Về Khóc Với Anh',
            author: 'Erik, Duzme Remix'
        },
        {
            id: "9",
            image:
                "https://photo-resize-zmp3.zmdcdn.me/w240_r1x1_jpeg/cover/b/1/a/a/b1aa55127e859a60291163b791000052.jpg",
            name: 'Chạy Về Khóc Với Anh',
            author: 'Erik, Duzme Remix'
        },
        {
            id: "10",
            image:
                "https://photo-resize-zmp3.zmdcdn.me/w240_r1x1_jpeg/cover/b/1/a/a/b1aa55127e859a60291163b791000052.jpg",
            name: 'Chạy Về Khóc Với Anh',
            author: 'Erik, Duzme Remix'
        },

    ]
    return (
        <div style={{
            marginTop: '38px',
            width: '300px'
        }}>
            <div
                style={{
                    marginBottom: '20px'
                }}>
                <Typography.Text
                    style={{
                        color: '#fff',
                        textTransform: 'uppercase',
                        fontSize: '12px',
                        letterSpacing: '1px',
                        fontWeight: '700',
                        opacity: '.7'
                    }}>
                    Bai hat dang phat
                </Typography.Text>
                <div
                    style={{
                        backgroundColor: 'rgb(94, 90, 84)',
                        borderRadius: '5px',
                        display: 'flex',
                        padding: '12px',
                        marginTop: '8px'
                    }}>
                    <div style={{ marginRight: '10px', }}>
                        <Image
                            preview={false}
                            src='https://photo-resize-zmp3.zmdcdn.me/w94_r1x1_webp/cover/5/5/4/2/554266220e2c71a9aca8ebdf2fc4d9e2.jpg'
                            style={{
                                width: '40px',
                                borderRadius: '5px',
                            }}>
                        </Image>
                    </div>
                    <div
                        style={{
                            display: 'flex',
                            flexDirection: 'column',
                            width: '80%',
                        }}>
                        <Typography.Text
                            style={{
                                fontWeight: '500',
                                fontSize: '14px',
                                color: 'white',
                                textOverflow: 'ellipsis',
                                overflow: 'hidden',
                                whiteSpace: 'nowrap'
                            }}>
                            Guong mat la lam lam lam lam lam lam lam
                        </Typography.Text>
                        <Typography.Text
                            style={{
                                fontSize: '12px',
                                color: 'rgba(254,255,255,.6)'
                            }}>
                            Mr.Siro
                        </Typography.Text>
                    </div>
                </div>
            </div>
            <ReactAudioPlayer
                src="my_audio_file.ogg"
                autoPlay
                controls
                style={{
                    backgroundColor: 'transparent'
                }}
            />
            <Typography.Text
                style={{
                    color: '#fff',
                    textTransform: 'uppercase',
                    fontSize: '12px',
                    letterSpacing: '1px',
                    fontWeight: '700',
                    opacity: '.7',
                    marginTop: '20px'
                }}>
                Tiep theo
            </Typography.Text>
            <div
                className='slider-bar'
                style={{
                    height: '400px',
                    overflow: 'auto',
                }}>
                {playList.map((item, key) => (
                    <Button
                        style={{
                            backgroundColor: 'rgb(94, 90, 84)',
                            borderRadius: '5px',
                            display: 'flex',
                            width: '295px',
                            height: '65px',
                            marginTop: '8px',
                            alignItems: 'center',
                        }}>
                        <div style={{ marginRight: '10px', }}>
                            <Image
                                src={item.image}
                                preview={false}
                                style={{
                                    width: '40px',
                                    borderRadius: '5px',
                                }}>
                            </Image>
                        </div>
                        <div
                            style={{
                                display: 'flex',
                                flexDirection: 'column',
                                width: '80%',
                            }}>
                            <Typography.Text
                                style={{
                                    fontWeight: '500',
                                    fontSize: '14px',
                                    color: 'white',
                                    textOverflow: 'ellipsis',
                                    overflow: 'hidden',
                                    whiteSpace: 'nowrap'
                                }}>
                                {item.name}
                            </Typography.Text>
                            <Typography.Text
                                style={{
                                    fontSize: '12px',
                                    color: 'rgba(254,255,255,.6)'
                                }}>
                                {item.author}
                            </Typography.Text>
                        </div>
                    </Button>
                ))}
            </div>
        </div>
    )
}

export default SongPlaying