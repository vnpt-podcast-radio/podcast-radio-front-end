import { Image, Typography } from "antd";
import { Header } from "antd/es/layout/layout";
import React, { createContext } from "react";
import SensorsIcon from "@mui/icons-material/Sensors";
import { EyeOutlined } from "@ant-design/icons";
import { HeartOutlined } from "@ant-design/icons";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
export const VisibleContent = createContext(true);
function HeaderRoom({ onClickClose }) {
    return (
        <Header
            style={{
                display: "flex",
                justifyContent: "space-between",
            }}
        >
            <div
                style={{
                    display: "flex",
                }}
            >
                <div
                    style={{
                        marginRight: "10px",
                    }}
                >
                    <Image
                        preview={false}
                        src="https://photo-resize-zmp3.zmdcdn.me/w240_r1x1_webp/cover/8/3/8/8/83882e4024f4ceb3dfe2d6b7c4250a14.jpg"
                        style={{
                            borderRadius: "999px",
                            width: "80px",
                        }}
                    ></Image>
                    <div
                        style={{
                            marginTop: "-14px",
                            display: "flex",
                            justifyContent: "center",
                            position: "relative",
                        }}
                    >
                        <div
                            style={{
                                backgroundColor: "red",
                                width: "70%",
                                borderRadius: "5px",
                            }}
                        >
                            <Typography.Text
                                style={{
                                    color: "white",
                                    display: "flex",
                                    justifyContent: "center",
                                    textTransform: "uppercase",
                                    fontSize: "12px",
                                    fontWeight: "700",
                                    letterSpacing: "1px",
                                }}
                            >
                                Live
                            </Typography.Text>
                        </div>
                    </div>
                </div>
                <div style={{}}>
                    <Typography.Title
                        level={1}
                        style={{
                            margin: 0,
                            color: "white",
                            marginBottom: "15px",
                        }}
                    >
                        ZingMp3 OnAir
                    </Typography.Title>
                    <div
                        style={{
                            display: "flex",
                            alignItems: "center",
                        }}
                    >
                        <SensorsIcon
                            style={{
                                marginRight: "6px",
                                color: "white",
                                fontSize: "20px",
                            }}
                        />
                        <Typography.Text
                            style={{
                                color: "white",
                                fontWeight: "500",
                                fontFamily: "Inter,sans-serif",
                            }}
                        >
                            BYE BYE STRESS!
                        </Typography.Text>
                    </div>
                </div>
                <div
                    style={{
                        display: "flex",
                        marginLeft: "32px",
                        marginTop: "8px",
                    }}
                >
                    <div style={{ marginRight: "15px" }}>
                        <EyeOutlined
                            style={{
                                padding: "6px",
                                marginRight: "6px",
                                color: "white",
                                fontSize: "16px",
                            }}
                        />
                        <Typography.Text
                            style={{
                                color: "white",
                                fontSize: "20px",
                                fontWeight: "500",
                            }}
                        >
                            89
                        </Typography.Text>
                    </div>
                    <div style={{}}>
                        <HeartOutlined
                            style={{
                                padding: "6px",
                                marginRight: "6px",
                                color: "white",
                                fontSize: "16px",
                            }}
                        />
                        <Typography.Text
                            style={{
                                color: "white",
                                fontSize: "20px",
                                fontWeight: "500",
                            }}
                        >
                            115.135
                        </Typography.Text>
                    </div>
                </div>
            </div>
            <a onClick={onClickClose}>
                <KeyboardArrowDownIcon
                    style={{
                        backgroundColor: "hsla(0,0%,100%,.2)",
                        fontSize: "30px",
                        padding: "5px",
                        color: "white",
                        borderRadius: "999px",
                    }}
                ></KeyboardArrowDownIcon>
            </a>
        </Header>
    );
}

export default HeaderRoom;
