import React from 'react'
import { Image, Typography, Input } from 'antd';
function Comment() {
    return (
        <div style={{
            width: '340px',
            padding: '20px',
            display: 'flex',
            backgroundColor: 'rgba(0,0,0,.5)',
            backdropFilter: 'blur(60px)',
            borderRadius: '12px',
            flexDirection: 'column',
            position: 'absolute',
            right: '30px',
            bottom: '30px',
            top: '94px',
        }}>
            <div>
                <Typography.Title
                    level={5}
                    style={{
                        color: 'white',
                        margin: 0,
                        display: 'flex',
                        justifyContent: 'center',
                        marginBottom: '12px'
                    }}>
                    Tro chuyen
                </Typography.Title>
            </div>
            <div>
                <div
                    className='slider-bar'
                    style={{
                        overflow: 'auto',
                        width: '100%',
                        height: '468px',
                        WebkitMaskImage: 'linear-gradient(transparent .5%,#000 10%)'
                    }}>
                    <div
                        style={{
                            marginTop: '16px',
                            display: 'flex',
                        }}>
                        <Image
                            preview={{
                                maskClassName: 'custom-mask',
                                mask: false,
                                visible: false,
                            }}
                            src='https://photo-resize-zmp3.zmdcdn.me/w320_r1x1_webp/avatars/b/0/d/d/b0dd70786bab81645d73e3b20002117d.jpg'
                            style={{
                                width: '38px',
                                borderRadius: '999px',
                                marginRight: '10px'
                            }}>
                        </Image>
                        <div
                            style={{
                                backgroundColor: 'hsla(0,0%,100%,.1)',
                                borderRadius: '12px',
                                flexGrow: 0,
                                width: 'auto',
                                padding: '8px 12px',
                            }}>
                            <Typography.Title
                                level={5}
                                style={{
                                    color: 'white',
                                    margin: 0
                                }}
                            >
                                Quach Quang Minh
                            </Typography.Title>

                            <Typography.Text
                                style={{
                                    color: 'white',

                                }}>
                                Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello
                            </Typography.Text>
                        </div>
                    </div>
                </div>
                <div
                    style={{
                        marginTop: '16px',
                        display: 'flex',
                    }}>
                    <Image
                        preview={{
                            maskClassName: 'custom-mask',
                            mask: false,
                            visible: false,
                        }}
                        src='https://photo-resize-zmp3.zmdcdn.me/w320_r1x1_webp/avatars/b/0/d/d/b0dd70786bab81645d73e3b20002117d.jpg'
                        style={{
                            width: '38px',
                            borderRadius: '999px'
                        }}>
                    </Image>
                    <Input
                        placeholder='Nhap binh luan vao day...'
                        style={{
                            marginLeft: '10px',
                            width: '100%',
                            color: 'black',
                        }}
                    >
                    </Input>
                </div>
            </div>
        </div>
    )
}

export default Comment