import { SearchOutlined, VerifiedUserOutlined } from '@mui/icons-material';
import { Dropdown, Layout, Menu, Image, Input, Avatar, Space, Typography } from 'antd';
import { DownOutlined } from '@ant-design/icons';
import { UserOutlined } from '@ant-design/icons';
import '../index';
import styled from 'styled-components';
const SiderbarDivide = styled.div`
height: 1px;
background-color: #847b5e4e;
`
const items = (
  <Menu mode='horizontal' style={{ right: 0, position: 'absolute', minWidth: '240px', maxWidth: '240px', minHeight: '216px', marginTop: '50px', flexDirection: 'column', display: 'flex', justifyContent: 'space-around' }}>
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
        Ant Group
      </a>
    </Menu.Item>
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer" href="https://ant.design">
        Ant Design
      </a>
    </Menu.Item>
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
        Aliyun
      </a>
    </Menu.Item>
    <SiderbarDivide />
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
        Aliyun
      </a>
    </Menu.Item>
  </Menu>

)

const { Header } = Layout;
const { Search } = Input;
const menu = (
  <Menu mode='horizontal' style={{ minWidth: '400px', maxWidth: '400px', minHeight: '90px', display: 'flex', marginTop: '50px', flexWrap: 'wrap' }}>

    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
        Ant Group
      </a>
    </Menu.Item>
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer" href="https://ant.design">
        Ant Design
      </a>
    </Menu.Item>
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
        Aliyun
      </a>
    </Menu.Item>
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
        Ant Group
      </a>
    </Menu.Item>
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer" href="https://ant.design">
        Ant Design
      </a>
    </Menu.Item>
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
        Aliyun
      </a>
    </Menu.Item>
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
        Ant Group
      </a>
    </Menu.Item>
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer" href="https://ant.design">
        Ant Design
      </a>
    </Menu.Item>
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
        Aliyun
      </a>
    </Menu.Item>
  </Menu>
);
function HeaderHome() {
  return (
    <Header style={{
      justifyItems: 'center',
      backgroundColor: 'transparent',
      textAlign: 'center',
      color: '#fff',
      height: '80px',
      paddingInline: 50,
      lineHeight: '15vh',
    }}>
      <div className="logo" />
      <div style={{ height: '80px', display: 'flex' }}>

        <div style={{ width: '50%', textAlign: 'center', alignSelf: 'center' }}>
          <Search style={{ display: 'flex' }} placeholder="input search text" enterButton />
        </div>

        <Menu style={{ width: '50%', height: '80px' }} mode="horizontal" >
          <Menu.Item key="1" style={{ alignSelf: 'center', width: '33%', textAlign: 'center' }}><Dropdown overlay={menu} placement="bottomCenter" defaultSelectedKeys={['1']}>
            <a >
              <Typography.Text style={{ color: 'white' }}>
                Hover me 1
              </Typography.Text><DownOutlined style={{ color: 'white' }} />
            </a>
          </Dropdown>
          </Menu.Item>
          <Menu.Item key="2" style={{ alignSelf: 'center', width: '33%', textAlign: 'center' }}><Dropdown overlay={menu} placement="bottomCenter">
            <a >
              <Typography.Text style={{ color: 'white' }}>
                Hover me 2
              </Typography.Text><DownOutlined style={{ color: 'white' }} />
            </a>
          </Dropdown>
          </Menu.Item>
          <Menu.Item key="3" style={{ alignSelf: 'center', width: '33%', textAlign: 'center' }}><Dropdown overlay={menu} placement="bottomCenter">
            <a >
              <Typography.Text style={{ color: 'white' }}>
                Hover me 3
              </Typography.Text><DownOutlined style={{ color: 'white' }} />
            </a>
          </Dropdown>
          </Menu.Item>
          {/* <Menu.Item focus key="6" style={{ textAlign: 'center', flexGrow: 2, alignSelf: 'center', justifySelf: 'center', justifyContent: 'center', justifyItems: 'center', alignContent: 'center' }}>
            <Search placeholder="input search text" enterButton />
          </Menu.Item> */}

        </Menu>
        <Menu style={{ position: 'relative', width: '15%', textAlign: 'center', alignSelf: 'center', justifySelf: 'center', justifyContent: 'center', justifyItems: 'center', alignContent: 'center' }}>
          <Dropdown overlay={items} trigger={['click']} placement="bottomCenter">
            <a>
              <Space wrap size={16}>
                <Avatar size={64} icon={<UserOutlined />} >

                </Avatar>
              </Space>
            </a>
          </Dropdown>

        </Menu>
      </div>


    </Header>
  );
}

export default HeaderHome;
