import { Layout, Menu, Image, Typography, Drawer } from "antd";
import { useState } from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { PlayCircleFilled } from "@ant-design/icons";
import { useNavigate, useParams } from "react-router";
import HeaderRoom from "../Room/HeaderRoom";
import Comment from "../Room/Comment";
import SongPlaying from "../Room/SongPlaying";
const playButton = (
  <PlayCircleFilled
    style={{
      fontSize: "40px",
    }}
  />
);
const { Content } = Layout;
const contentStyle = {
  textAlign: "center",
  minHeight: "90vh",
  lineHeight: "90vh",
  position: "relative",
  color: "#fff",
  flexGrow: 1,
};
function HomeContent() {
  const path = useParams();
  console.log(path.roomId);
  const navigate = useNavigate();
  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 5,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 7,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  };
  const responsiveCatgory = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 5,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 5,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  };
  const liveChannels = [
    {
      id: "1",
      image:
        "https://photo-resize-zmp3.zmdcdn.me/w240_r1x1_webp/cover/8/3/8/8/83882e4024f4ceb3dfe2d6b7c4250a14.jpg",
    },
    {
      id: "2",
      image:
        "https://photo-resize-zmp3.zmdcdn.me/w240_r1x1_webp/cover/8/3/8/8/83882e4024f4ceb3dfe2d6b7c4250a14.jpg",
    },
    ,
    {
      id: "3",
      image:
        "https://photo-resize-zmp3.zmdcdn.me/w240_r1x1_webp/cover/8/3/8/8/83882e4024f4ceb3dfe2d6b7c4250a14.jpg",
    },
    {
      id: "4",
      image:
        "https://photo-resize-zmp3.zmdcdn.me/w240_r1x1_webp/cover/8/3/8/8/83882e4024f4ceb3dfe2d6b7c4250a14.jpg",
    },
    {
      id: "5",
      image:
        "https://photo-resize-zmp3.zmdcdn.me/w240_r1x1_webp/cover/8/3/8/8/83882e4024f4ceb3dfe2d6b7c4250a14.jpg",
    },
    {
      id: "6",
      image:
        "https://photo-resize-zmp3.zmdcdn.me/w240_r1x1_webp/cover/8/3/8/8/83882e4024f4ceb3dfe2d6b7c4250a14.jpg",
    },
    {
      id: "7",
      image:
        "https://photo-resize-zmp3.zmdcdn.me/w240_r1x1_webp/cover/8/3/8/8/83882e4024f4ceb3dfe2d6b7c4250a14.jpg",
    },
  ];
  const [visible, setVisible] = useState(false);
  const showDrawer = (id) => {
    navigate(`${id}`);
    setVisible(true);
  };
  const onClose = () => {
    navigate("");
    setVisible(false);
  };
  return (
    <Content style={contentStyle}>
      <Menu
        style={{
          padding: "0 59px",
          margin: "10px",
        }}
      >
        <div>
          <Carousel responsive={responsive}>
            {liveChannels.map((item, key) => (
              <div
                key={key}
                style={{
                  paddingTop: "68px",
                  width: "153px",
                }}
              >
                <div
                  style={{
                    position: "relative",
                  }}
                >
                  <div
                    style={{
                      margin: "auto",
                      display: "flex",
                      overflow: "hidden",
                      borderRadius: "999px",
                    }}
                  >
                    <a
                      onClick={() => showDrawer(item.id)}
                      className="img_scale"
                    >
                      <Image
                        preview={{
                          maskClassName: "custom-mask",
                          mask: playButton,
                          visible: false,
                        }}
                        src={item.image}
                      ></Image>
                    </a>
                  </div>
                  <div
                    style={{
                      position: "absolute",
                      top: "64%",
                      left: "64%",
                    }}
                  >
                    <Image
                      preview={false}
                      src="https://photo-resize-zmp3.zmdcdn.me/w240_r1x1_webp/cover/8/3/8/8/83882e4024f4ceb3dfe2d6b7c4250a14.jpg"
                      style={{
                        borderRadius: "999px",
                      }}
                    ></Image>
                  </div>
                </div>
                <div
                  style={{
                    marginTop: "-10px",
                    display: "flex",
                    justifyContent: "center",
                    position: "relative",
                  }}
                >
                  <div
                    style={{
                      backgroundColor: "red",
                      width: "30%",
                      borderRadius: "5px",
                    }}
                  >
                    <Typography.Text
                      style={{
                        color: "white",
                        display: "flex",
                        justifyContent: "center",
                        textTransform: "uppercase",
                        fontSize: "10px",
                        fontWeight: "700",
                        letterSpacing: "1px",
                      }}
                    >
                      Live
                    </Typography.Text>
                  </div>
                </div>

                <div
                  style={{
                    margin: "auto",
                    width: "153px",
                    marginTop: "20px",
                    height: "42px",
                  }}
                >
                  <Typography.Title
                    level={3}
                    style={{
                      color: "white",
                      fontSize: "16px",
                    }}
                  >
                    XONERadio
                  </Typography.Title>
                  <Typography.Text
                    level={3}
                    style={{
                      color: "white",
                      fontSize: "12px",
                      color: "hsla(0,0%,100%,0.5)",
                      lineHeight: 0,
                    }}
                  >
                    80 nguoi nghe
                  </Typography.Text>
                </div>
              </div>
            ))}
          </Carousel>
          ;
        </div>
        <div
          style={{
            marginTop: "48px",
          }}
        >
          <Typography.Title
            style={{
              display: "flex",
              justifyContent: "flex-start",
              color: "white",
              margin: 0,
              fontWeight: "bold",
              marginBottom: "20px",
            }}
            level={4}
          >
            Rank
          </Typography.Title>

          <Menu.ItemGroup
            className="slider-bar"
            style={{
              backgroundColor: "hsla(0, 0%, 100%, 0.1)",
              borderRadius: "5px",
              width: "70%",
              maxHeight: "370px",
              overflow: "overlay",
            }}
          >
            <Menu.Item
              style={{
                height: "100px",
                display: "flex",
                width: "800px",
                marginBottom: "17px",
              }}
            >
              <div style={{ display: "flex", alignItems: "center" }}>
                <Typography.Title
                  level={5}
                  style={{
                    color: "white",
                    textAlign: "center",
                    margin: 0,
                    width: "100px",
                  }}
                >
                  1
                </Typography.Title>
                <Image
                  preview={false}
                  src="https://photo-resize-zmp3.zmdcdn.me/w128_r1x1_webp/images/5/9/7/e/597e53b7433a722c4204ab4f280559ca.jpg"
                  style={{
                    width: "100px",
                    marginLeft: "30px",
                  }}
                ></Image>
                <div
                  style={{
                    display: "flex",
                    marginLeft: "90px",
                    alignItems: "baseline",
                    flexDirection: "column",
                  }}
                >
                  <Typography.Title
                    level={4}
                    style={{
                      color: "white",
                      textAlign: "center",
                      margin: 0,
                    }}
                  >
                    Vietcetera
                  </Typography.Title>
                  <Typography.Text
                    level={6}
                    style={{
                      color: "white",
                      textAlign: "center",
                      color: "hsla(0,0%,100%,0.5)",
                    }}
                  >
                    Follow: 1000
                  </Typography.Text>
                  <Typography.Text
                    level={6}
                    style={{
                      color: "white",
                      textAlign: "center",
                      color: "hsla(0,0%,100%,0.5)",
                    }}
                  >
                    Tri ki cam xuc
                  </Typography.Text>
                  <Typography.Text
                    level={6}
                    style={{
                      color: "white",
                      textAlign: "center",
                      color: "hsla(0,0%,100%,0.5)",
                    }}
                  >
                    03/01/2023
                  </Typography.Text>
                </div>
              </div>
            </Menu.Item>
          </Menu.ItemGroup>
        </div>
        <div
          style={{
            marginTop: "48px",
          }}
        >
          <div
            style={{
              position: "relative",
              width: "100%",
            }}
          >
            <div
              style={{
                padding: "0 0 20px",
                position: "relative",
                display: "flex",
                alignItems: "center",
              }}
            >
              <div
                style={{
                  marginRight: "10px",
                  overflow: "hidden",
                  display: "flex",
                  borderRadius: "5px",
                  width: "50px",
                }}
              >
                <a
                  className="img_scale"
                  style={{
                    textDecoration: "none",
                  }}
                >
                  <Image
                    preview={{
                      maskClassName: "custom-mask",
                      mask: false,
                      visible: false,
                    }}
                    src="https://photo-resize-zmp3.zmdcdn.me/w240_r1x1_webp/avatars/2/c/c/f/2ccf96f6da468b466c5f8d1188f62eee.jpg"
                  ></Image>
                </a>
              </div>
              <div>
                <Typography.Text
                  style={{
                    display: "flex",
                    justifyContent: "flex-start",
                    color: "white",
                    margin: 0,
                    fontSize: "14px",
                    textTransform: "uppercase",
                    color: "hsla(0,0%,100%,0.5)",
                    fontWeight: 500,
                  }}
                >
                  podcast
                </Typography.Text>
                <Typography.Title
                  style={{
                    display: "flex",
                    justifyContent: "flex-start",
                    color: "white",
                    margin: 0,
                    fontWeight: "bold",
                  }}
                  level={4}
                >
                  Pladio
                </Typography.Title>
              </div>
            </div>
            <div
              style={{
                width: "100%",
              }}
            >
              <Carousel responsive={responsiveCatgory}>
                <div
                  style={{
                    width: "213px",
                  }}
                >
                  <div
                    style={{
                      position: "relative",
                    }}
                  >
                    <div
                      style={{
                        margin: "auto",
                        display: "flex",
                        overflow: "hidden",
                        borderRadius: "5px",
                      }}
                    >
                      <a className="img_scale">
                        <Image
                          preview={{
                            maskClassName: "custom-mask",
                            mask: false,
                            visible: false,
                          }}
                          src="https://photo-resize-zmp3.zmdcdn.me/w320_r1x1_webp/avatars/b/0/d/d/b0dd70786bab81645d73e3b20002117d.jpg"
                        ></Image>
                      </a>
                    </div>
                  </div>
                  <Typography.Title
                    level={5}
                    style={{
                      textTransform: "none",
                      color: "white",
                      margin: "8px 0 4px",
                      display: "flex",
                      justifyContent: "flex-start",
                    }}
                  >
                    How2Money x Doctor
                  </Typography.Title>
                </div>
              </Carousel>
              ;
            </div>
          </div>
        </div>
      </Menu>
      <Drawer
        style={{
          backgroundImage:
            'url("https://photo-zmp3.zmdcdn.me/cover_rect/d/6/7/4/d6746d1bbff609111133449abb9e622b.jpg")',
          backgroundSize: "cover",
          padding: "30px 30px 30px 40px",
        }}
        bodyStyle={{
          padding: 0,
        }}
        placement="bottom"
        closable={false}
        visible={visible}
        height="100%"
        width={"100%"}
      >
        <HeaderRoom onClickClose={onClose} />
        <SongPlaying />
        <Comment />
      </Drawer>
    </Content>
  );
}

export default HomeContent;
