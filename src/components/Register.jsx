import { Button, Checkbox, Form, Input, Typography } from "antd";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { useState } from "react";
import HCaptcha from "@hcaptcha/react-hcaptcha";

const StyledContainer = styled.div`
  position: absolute;
  left: -10%;
  width: 120%;
  height: 200vh;
  background: url(https://cdn.discordapp.com/attachments/1034369224543567936/1085448736525791282/bg.jpg);
`;

const StyledScene = styled.div`
  position: absolute;
  inset: 0;
`;

const StyledLayer = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 200vh;
`;

const StyledRegister = styled.div`
  position: relative;
  top: 25%;
  padding: 60px;
  background: rgba(255, 255, 255, 0.15);
  backdrop-filter: blur(25px);
  border: 1px solid rgba(255, 255, 255, 0.5);
  border-bottom: 1px solid rgba(255, 255, 255, 0.25);
  border-left: 1px solid rgba(255, 255, 255, 0.25);
  border-radius: 20px;
  width: 500px;
  display: flex;
  flex-direction: column;
  gap: 30px;
  box-shadow: 0 25px 50px rgba(0, 0, 0, 0.1);
`;

const StyledInputBox = styled.div`
  position: relative;
`;

const styles = {
  section: {
    position: "relative",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    height: "100vh",
    overflowX: "hidden",
  },

  imgLayer: {
    position: "absolute",
    bottom: 0,
    left: 0,
    width: "100%",
    height: "100%",
    objectFit: "cover",
  },

  titleRegister: {
    position: "relative",
    width: "100%",
    textAlign: "center",
    fontSize: "2.5em",
    fontWeight: 600,
    color: "#fff",
    marginBottom: "10px",
  },

  inputBox: {
    position: "relative",
    width: "100%",
    padding: "15px 20px",
    outline: "none",
    fontSize: "1.25em",
    color: "#000",
    borderRadius: "5px",
    boxShadow: "inset 0 0 15px rgba(0, 0, 0, 0.25)",
    border: "1px solid rgba(0, 0, 0, 0.5)",
  },

  btnSubmit: {
    position: "relative",
    width: "100%",
    height: "100%",
    padding: "15px 20px",
    outline: "none",
    border: "none",
    fontWeight: 500,
    boxShadow: "none",
  },

  textLink: {
    fontSize: "1.25em",
    color: "#fff",
    textDecoration: "none",
  },

  textLinkService: {
    color: "#fff",
    textDecoration: "none",
  },
};

function Register() {
  const [nameUser, setNameUser] = useState("");
  const [email, setEmail] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [captchaToken, setCaptchaToken] = useState(null);

  const hasNumber = /\d/;
  const hasUpperCase = /[A-Z]/;
  const hasLowerCase = /[a-z]/;
  const hasSpecialCharacter = /[!@#$%^&*(),.?":{}|<>]/;

  const PasswordValidator = (_, value) => {
    if (!value) {
      return Promise.reject("");
    }
    if (value.length < 8) {
      return Promise.reject("Mật khẩu phải có ít nhất 8 ký tự");
    }
    if (!hasNumber.test(value)) {
      return Promise.reject("Mật khẩu phải chứa ít nhất 1 số");
    }
    if (!hasUpperCase.test(value)) {
      return Promise.reject("Mật khẩu phải chứa ít nhất 1 chữ cái viết hoa");
    }
    if (!hasLowerCase.test(value)) {
      return Promise.reject("Mật khẩu phải chứa ít nhất 1 chữ cái viết thường");
    }
    if (!hasSpecialCharacter.test(value)) {
      return Promise.reject("Mật khẩu phải chứa ít nhất 1 ký tự đặc biệt");
    }
    return Promise.resolve();
  };

  const validatePhoneNumber = (rule, value, callback) => {
    const phoneNumberRegex = /^(0|\+84)[3|5|7|8|9][0-9]{8}$/;
    if (!phoneNumberRegex.test(value)) {
      callback("Số điện thoại không hợp lệ");
    } else {
      callback();
    }
  };

  const handleCaptchaVerify = (token) => {
    setCaptchaToken(token);
  };

  const handleRegistration = (event) => {
    // event.preventDefault();
    if (!captchaToken) {
      return;
    }
  };

  return (
    <>
      <section style={styles.section}>
        <StyledContainer>
          <StyledScene id="scene">
            <StyledLayer>
              <img
                src="https://cdn.discordapp.com/attachments/1034369224543567936/1085448736752271390/moon.png"
                alt=""
                style={styles.imgLayer}
              />
            </StyledLayer>
            <StyledLayer>
              <img
                src="https://cdn.discordapp.com/attachments/1034369224543567936/1085448737272365066/mountains02.png"
                alt=""
                style={styles.imgLayer}
              />
            </StyledLayer>
            <StyledLayer>
              <img
                src="https://cdn.discordapp.com/attachments/1034369224543567936/1085448736987164712/mountains01.png"
                alt=""
                style={styles.imgLayer}
              />
            </StyledLayer>
            <StyledLayer>
              <img
                src="https://cdn.discordapp.com/attachments/1034369224543567936/1085448737565974568/road.png"
                alt=""
                style={styles.imgLayer}
              />
            </StyledLayer>
          </StyledScene>
        </StyledContainer>

        <StyledRegister>
          <Typography.Title level={2} style={styles.titleRegister}>
            Đăng Ký
          </Typography.Title>

          <Form scrollToFirstError>
            <StyledInputBox>
              <Form.Item
                label=""
                name="name"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập họ tên của bạn",
                  },
                ]}
              >
                <Input
                  placeholder="Họ tên"
                  onChange={(e) => setNameUser(e.target.value)}
                  style={styles.inputBox}
                />
              </Form.Item>
            </StyledInputBox>

            <StyledInputBox>
              <Form.Item
                label=""
                name="phone"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập số điện thoại của bạn",
                  },
                  {
                    validator: validatePhoneNumber,
                  },
                ]}
              >
                <Input
                  placeholder="Điện thoại"
                  type="number"
                  onKeyPress={(event) => {
                    if (!/[0-9]/.test(event.key)) {
                      event.preventDefault();
                    }
                  }}
                  onChange={(e) => setPhoneNumber(e.target.value)}
                  style={styles.inputBox}
                />
              </Form.Item>
            </StyledInputBox>

            <StyledInputBox>
              <Form.Item
                label=""
                name="email"
                rules={[
                  {
                    type: "email",
                    message: "Địa chỉ email không hợp lệ",
                  },
                  {
                    required: true,
                    message: "Vui lòng nhập email của bạn",
                  },
                ]}
              >
                <Input
                  placeholder="Email"
                  onChange={(e) => setEmail(e.target.value)}
                  style={styles.inputBox}
                />
              </Form.Item>
            </StyledInputBox>

            <StyledInputBox>
              <Form.Item
                label=""
                name="password"
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập mật khẩu của bạn",
                  },
                  {
                    validator: PasswordValidator,
                  },
                ]}
              >
                <Input.Password
                  placeholder="Mật khẩu"
                  onChange={(e) => setPassword(e.target.value)}
                  style={styles.inputBox}
                />
              </Form.Item>
            </StyledInputBox>

            <StyledInputBox>
              <Form.Item
                label=""
                name="confirm password"
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập xác nhận mật khẩu",
                  },
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      if (!value || getFieldValue("password") === value) {
                        return Promise.resolve();
                      }
                      return Promise.reject(
                        new Error("Xác nhận mật khẩu và mật khẩu không khớp")
                      );
                    },
                  }),
                ]}
              >
                <Input.Password
                  placeholder="Xác nhận mật khẩu"
                  onChange={(e) => setConfirmPassword(e.target.value)}
                  style={styles.inputBox}
                />
              </Form.Item>
            </StyledInputBox>

            <Form.Item
              name="terms of service"
              rules={[
                {
                  validator: (_, value) =>
                    value
                      ? Promise.resolve()
                      : Promise.reject(
                          new Error(
                            "Bạn phải đồng ý với Điều khoản dịch vụ của chúng tôi"
                          )
                        ),
                },
              ]}
            >
              <Checkbox style={styles.textLinkService}>
                Bằng cách đăng ký, bạn đồng ý với{" "}
                <Link href="#">Điều khoản dịch vụ của chúng tôi</Link>
              </Checkbox>
            </Form.Item>

            <Form.Item label="Captcha" name="captcha">
              <HCaptcha
                sitekey="05cec149-46e6-43ed-a8db-fa56a2781f87"
                onVerify={handleCaptchaVerify}
              />
            </Form.Item>

            <StyledInputBox>
              <Form.Item>
                <Button
                  type="primary"
                  htmlType="submit"
                  onClick={handleRegistration}
                  style={styles.btnSubmit}
                  disabled={!captchaToken}
                >
                  ĐĂNG KÝ TÀI KHOẢN
                </Button>
              </Form.Item>
            </StyledInputBox>

            <Typography.Paragraph style={{ textAlign: "center" }}>
              <Typography.Text style={styles.textLink}>
                Bạn đã có tài khoản? <Link to="/login">Đăng nhập!</Link>
              </Typography.Text>
            </Typography.Paragraph>
          </Form>
        </StyledRegister>
      </section>
    </>
  );
}

export default Register;
