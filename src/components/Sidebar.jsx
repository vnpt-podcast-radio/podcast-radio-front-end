import { Menu, Typography, Image, Button } from 'antd';
import { HomeOutlined } from '@ant-design/icons';
import Sider from 'antd/es/layout/Sider';
import styled from 'styled-components';
import ListAltIcon from '@mui/icons-material/ListAlt';
import BarChartIcon from '@mui/icons-material/BarChart';
function SidebarHome() {
    const SiderbarDivide = styled.div`
    height: 1px;
    background-color: #847b5e4e;
    `
    const StyleLogo = styled.div`
    display: flex;
    `
    return (
        <Sider width={'240px'} trigger={null} style={{ backgroundColor: 'hsla(0,0%,100%,0.05)' }}>
            <Menu mode="inline" style={{ borderRight: 'none' }}>
                <Menu.Item style={{ height: '120px', width: '100%', padding: '0px', margin: '0px' }} key="home">
                    <StyleLogo>
                        <Typography.Link href='/home' alt="Trang chủ">
                            <Image preview={false} style={{ height: 60, }} src="https://cdn.discordapp.com/attachments/1034369224543567936/1075374315580694528/logo.png" alt="Spotify Logo" />
                        </Typography.Link>

                        <Typography.Text style={{
                            fontFamily: "Kanit, sans-serif",
                            fontWeight: "900",
                            fontSize: "20px",
                            textAlign: 'start',
                            color: 'white'
                        }}>
                            VNPT<br /> Podcast
                        </Typography.Text>
                    </StyleLogo>
                </Menu.Item>
                <Menu.ItemGroup>
                    <Menu.Item style={{ color: 'white' }} key="1" icon={<HomeOutlined />}>
                        <Typography.Link style={{ margin: 0, marginLeft: '5px' }}>
                            Trang chủ
                        </Typography.Link>
                    </Menu.Item>
                    <Menu.Item style={{ color: 'white' }} key="2" icon={<BarChartIcon />}>
                        <Typography.Link style={{ margin: 0 }}>
                            Xếp hạng
                        </Typography.Link>
                    </Menu.Item>
                    <Menu.Item style={{ color: 'white' }} key="3" icon={<ListAltIcon />}>
                        <Typography.Link style={{ margin: 0 }}>
                            Theo dõi
                        </Typography.Link>
                    </Menu.Item>
                </Menu.ItemGroup>
                <SiderbarDivide />
                <Menu.ItemGroup className='slider-bar'
                    style={{
                        height: '300px',
                        overflow: 'auto',
                        WebkitMaskImage: 'linear-gradient(transparent .5%,#000 10%)'
                    }}>
                    <Menu.Item style={{ color: 'white' }} key="4" icon={<HomeOutlined />}>
                        <Typography.Link style={{ margin: 0 }}>
                            Trang chủ
                        </Typography.Link>
                    </Menu.Item>
                    <Menu.Item style={{ color: 'white' }} key="5" icon={<BarChartIcon />}>
                        <Typography.Link style={{ margin: 0 }}>
                            Xếp hạng
                        </Typography.Link>
                    </Menu.Item>
                    <Menu.Item style={{ color: 'white' }} key="6" icon={<ListAltIcon />}>
                        <Typography.Link style={{ margin: 0 }}>
                            Theo dõi
                        </Typography.Link>
                    </Menu.Item>
                    <Menu.Item style={{ color: 'white' }} key="7" icon={<HomeOutlined />}>
                        <Typography.Link style={{ margin: 0 }}>
                            Trang chủ
                        </Typography.Link>
                    </Menu.Item>
                    <Menu.Item style={{ color: 'white' }} key="8" icon={<BarChartIcon />}>
                        <Typography.Link style={{ margin: 0 }}>
                            Xếp hạng
                        </Typography.Link>
                    </Menu.Item>
                    <Menu.Item style={{ color: 'white' }} key="9" icon={<ListAltIcon />}>
                        <Typography.Link style={{ margin: 0 }}>
                            Theo dõi
                        </Typography.Link>
                    </Menu.Item>
                    <Menu.Item style={{ color: 'white' }} key="10" icon={<HomeOutlined />}>
                        <Typography.Link style={{ margin: 0 }}>
                            Trang chủ
                        </Typography.Link>
                    </Menu.Item>
                    <Menu.Item style={{ color: 'white' }} key="11" icon={<BarChartIcon />}>
                        <Typography.Link style={{ margin: 0 }}>
                            Xếp hạng
                        </Typography.Link>
                    </Menu.Item>
                    <Menu.Item style={{
                        textAlign: 'center',
                        backgroundImage: 'linear-gradient(117deg,#5a4be7,#c86dd7 102%)', minHeight: '120px',
                        padding: '15px 8px',
                        borderRadius: '8px',
                        margin: '10px 0'
                    }} key="12">
                        <Typography.Text style={{ margin: 0, color: 'white' }}>
                            Nạp tiền để có thể sử dụng nhiều dịch vụ khác của web
                        </Typography.Text>
                        <Button style={{ marginTop: '10px' }}><Typography.Link style={{ margin: '10px', paddingTop: '5px', }}>
                            Nạp tiền
                        </Typography.Link></Button>
                    </Menu.Item>
                </Menu.ItemGroup>
                <SiderbarDivide />
            </Menu>
        </Sider>
    );
}

export default SidebarHome;