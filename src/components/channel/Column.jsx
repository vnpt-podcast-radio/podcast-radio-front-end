import { Typography } from 'antd'
import React from 'react'
import { Draggable, Droppable } from 'react-beautiful-dnd'
function Column({ column, tasks }) {
    return (
        <div style={{ marginLeft: '15px', marginRight: '15px', border: '3px', width: '400px', height: '580px', display: 'flex', flexDirection: 'column', backgroundColor: '#16181c' }}>
            <div style={{ height: '60px', backgroundColor: '#1b1d21', padding: '15px', border: '3px 3px 0 0', marginBottom: '1rem' }}>
                <Typography.Text style={{ color: 'white', fontWeight: 600, fontSize: '17px', color: 'rgba(255, 255, 255, 0.5)' }}>
                    {column.title}
                </Typography.Text>
            </div>
            <Droppable droppableId={column.id}>
                {(droppableProvided, droppableSnapshot) =>
                    <div
                        className='slider-bar'
                        ref={droppableProvided.innerRef}
                        {...droppableProvided.droppableProps}
                        style={{
                            paddingLeft: '1.5rem',
                            paddingRight: '1.5rem',
                            display: 'flex',
                            flexDirection: 'column',
                            overflow: 'auto'
                        }}>
                        {tasks.map((task, index) => (
                            <Draggable
                                key={task.id}
                                draggableId={`${task.id}`}
                                index={index}>
                                {(draggableProvided, draggableSnapshot) => (
                                    <div
                                        ref={draggableProvided.innerRef}
                                        {...draggableProvided.draggableProps}
                                        {...draggableProvided.dragHandleProps}
                                        style={{
                                            marginBottom: '1rem',
                                            backgroundColor: '#262730',
                                            padding: '1.5rem',
                                            border: '3px',
                                            height: '72px',
                                            outline: "2px solid",
                                        }}>
                                        <Typography.Text style={{
                                            color: 'white'
                                        }}>
                                            {task.content}
                                        </Typography.Text>

                                    </div>
                                )}
                            </Draggable>

                        ))}
                    </div>}
            </Droppable>
        </div>
    )
}

export default Column