import { Button, Image, Typography, Menu } from 'antd';
import { Header } from 'antd/es/layout/layout'
import React from 'react'
import styled from 'styled-components';
import { Tabs } from 'antd';
import ListContent from './ListContent';
import IntroduceContent from './IntroduceContent';
import { useNavigate, useParams } from 'react-router';
import TabPane from 'antd/es/tabs/TabPane';
function ChannelHeader({ onClickOpen }) {
    const path = useParams();
    console.log(path.key);
    const navigate = useNavigate();
    const SiderbarDivide = styled.div`
    height: 1px;
    background-color: #847b5e4e;
    `
    return (
        <Header
            style={{
                padding: 0,
                display: 'flex',
                justifyContent: "space-between",
                backgroundColor: 'transparent',
                marginBottom: '50px',
                flexDirection: 'column'
            }}>
            <div
                style={{
                    display: 'flex', justifyContent: 'space-between', width: '100%', marginBottom: '20px'
                }}>
                <div style={{ display: 'flex' }}>
                    <div
                        style={{
                            marginRight: '10px'
                        }}>
                        <Image
                            preview={false}
                            src='https://photo-resize-zmp3.zmdcdn.me/w240_r1x1_webp/cover/8/3/8/8/83882e4024f4ceb3dfe2d6b7c4250a14.jpg'
                            style={{
                                borderRadius: '999px',
                                width: '80px',
                            }}>
                        </Image>
                    </div>
                    <div
                        style={{
                        }}>
                        <Typography.Title
                            level={1}
                            style={{
                                margin: 0,
                                color: 'white',
                                marginBottom: '15px'
                            }}>
                            ZingMp3 OnAir
                        </Typography.Title>
                        <div
                            style={{
                                display: 'flex',
                                alignItems: 'center'
                            }}>
                            <Typography.Text
                                style={{
                                    color: 'white',
                                    fontWeight: '500',
                                    fontFamily: 'Inter,sans-serif'
                                }}>
                                @user-qy9zj7hr1v
                            </Typography.Text>
                        </div>
                    </div>
                </div>
                <Button onClick={onClickOpen} style={{ backgroundColor: 'rgba(255, 255, 255, 0.2)' }}>
                    <Typography.Text style={{ color: 'white' }}>
                        Tao phien truc tiep
                    </Typography.Text>
                </Button>
            </div>
            <Menu style={{ color: 'white', height: '50px' }} mode="horizontal">
                <Tabs
                    style={{ width: '100%', color: 'white' }}
                    defaultActiveKey={path.key}
                    onChange={
                        key => {
                            console.log(key)
                            navigate(`/channel/${key}`)
                        }
                    }
                >
                    <TabPane tab="Danh sach phat" key='list'>
                        <ListContent></ListContent>
                    </TabPane>
                    <TabPane tab="Gioi thieu" key='intro'>
                        <IntroduceContent></IntroduceContent>
                    </TabPane>
                </Tabs>
            </Menu>
            <SiderbarDivide />
        </Header >
    )
}

export default ChannelHeader