import { Typography } from 'antd'
import React from 'react'
import styled from 'styled-components';
function IntroduceContent() {
    const SiderbarDivide = styled.div`
    height: 1px;
    background-color: #847b5e4e;
    `
    return (
        <div
            style={{
                display: 'flex',
            }}>
            <div
                style={{
                    flex: 2,
                    paddingRight: '96px'
                }}>
                <div>
                    <div style={{
                        paddingBottom: '32px'
                    }}>
                        <Typography.Title
                            level={5}
                            style={{ color: 'white' }}>
                            Mo ta
                        </Typography.Title>
                        <Typography.Text
                            style={{ color: 'white' }}>
                            Hello minh la Minh
                        </Typography.Text>
                    </div>
                    <SiderbarDivide />


                </div>
                <div>
                    <div style={{
                        paddingBottom: '32px'
                    }}>
                        <Typography.Title
                            level={5}
                            style={{ color: 'white' }}>
                            Chi tiet
                        </Typography.Title>
                        <Typography.Text
                            style={{ color: 'white' }}>
                            Hello minh la Minh
                        </Typography.Text>
                    </div>
                    <SiderbarDivide />


                </div>
            </div>
            <div
                style={{
                    flex: 1
                }}>
                <div
                    style={{
                        paddingBottom: '12px',
                    }}>
                    <Typography.Title
                        level={5}
                        style={{ color: 'white', }}>
                        Thong ke
                    </Typography.Title>
                    <SiderbarDivide />
                    <Typography.Text
                        style={{
                            color: 'white',
                            paddingTop: '12px',
                        }}>
                        Đã tham gia 10 thg 5, 2018
                    </Typography.Text>
                </div>

                <SiderbarDivide />
            </div>
        </div>
    )
}

export default IntroduceContent