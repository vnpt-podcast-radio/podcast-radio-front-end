import React from "react";
import { Image, Typography } from "antd";
import { useState } from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
function ListContent() {
    const responsive = {
        superLargeDesktop: {
            breakpoint: { max: 4000, min: 3000 },
            items: 5,
        },
        desktop: {
            breakpoint: { max: 3000, min: 1024 },
            items: 4,
        },
        tablet: {
            breakpoint: { max: 1024, min: 464 },
            items: 2,
        },
        mobile: {
            breakpoint: { max: 464, min: 0 },
            items: 1,
        },
    };
    const [visible, setVisible] = useState(false);
    const showDrawer = () => {
        setVisible(true);
    };
    return (
        <div
            style={{
                height: "400px",
                alignItems: "center",
            }}
        >
            <Typography.Title style={{ color: "white" }}>
                Danh sach da phat
            </Typography.Title>
            <Carousel responsive={responsive}>
                <div
                    style={{
                        display: "flex",
                        backgroundColor: "#545151",
                        padding: "10px",
                        borderRadius: "10px",
                        margin: "10px",
                    }}
                >
                    <div
                        style={{
                            position: "relative",
                        }}
                    >
                        <div
                            style={{
                                margin: "auto",
                                display: "flex",
                                overflow: "hidden",
                                borderRadius: "5px",
                            }}
                        >
                            <a onClick={showDrawer}>
                                <Image
                                    preview={false}
                                    src="https://photo-resize-zmp3.zmdcdn.me/w240_r1x1_webp/cover/8/3/8/8/83882e4024f4ceb3dfe2d6b7c4250a14.jpg"
                                ></Image>
                            </a>
                        </div>
                    </div>

                    <div
                        style={{
                            width: "220px",
                            height: "42px",
                            marginLeft: "10px",
                        }}
                    >
                        <Typography.Title
                            level={3}
                            style={{
                                color: "white",
                                fontSize: "16px",
                                margin: "0",
                            }}
                        >
                            XONERadio radio radio radio
                        </Typography.Title>
                        <Typography.Text
                            level={3}
                            style={{
                                color: "white",
                                fontSize: "12px",
                                color: "hsla(0,0%,100%,0.5)",
                                lineHeight: 0,
                            }}
                        >
                            80 nguoi nghe
                        </Typography.Text>
                    </div>
                </div>
            </Carousel>
        </div>
    );
}

export default ListContent;
